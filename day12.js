let reader = require("line-reader");
function parseInput(path) {
	let puzzle_input = {};
	reader.eachLine(path, function (line, last) {
		let n1 = line.split("-")[0];
		let n2 = line.split("-")[1];
		if (puzzle_input[n1] === undefined) {
			puzzle_input[n1] = [];
		}
		if (puzzle_input[n2] === undefined) {
			puzzle_input[n2] = [];
		}
		puzzle_input[n1].push({ id: n2, big: n2 == n2.toUpperCase() });
		puzzle_input[n2].push({ id: n1, big: n1 == n1.toUpperCase() });
		if (last) {
			printAnswers(puzzle_input);
		}
	});
}
let totalPathsP1 = [];
function exploreNodeP1(input, node, currentPath) {
	let curr_path = [...currentPath];
	curr_path.push(node.id);
	if (node.id == "end") {
		totalPathsP1.push(curr_path);
	} else {
		for (let index = 0; index < input[node.id].length; index++) {
			if (
				input[node.id][index].big ||
				!curr_path.includes(input[node.id][index].id)
			) {
				exploreNodeP1(input, input[node.id][index], curr_path);
			}
		}
	}
}

let totalPathsP2 = [];

function alreadyDoubles(array) {
	for (let index = 0; index < array.length; index++) {
		const element = array[index];
		if (
			array.reduce((total, x) => (x == element ? total + 1 : total), 0) >=
				2 &&
			array[index].toLowerCase() == array[index]
		) {
			return true;
		}
	}
	return false;
}

function exploreNodeP2(input, node, currentPath) {
	let curr_path = [...currentPath];
	curr_path.push(node.id);
	if (node.id == "end") {
		totalPathsP2.push(curr_path);
	} else {
		for (let index = 0; index < input[node.id].length; index++) {
			if (
				input[node.id][index].id !== "start" &&
				(input[node.id][index].big ||
					!(
						alreadyDoubles(curr_path) &&
						curr_path.includes(input[node.id][index].id)
					))
			) {
				exploreNodeP2(input, input[node.id][index], curr_path);
			}
		}
	}
}

function solvePartOne(puzzle_input) {
	let input = JSON.parse(JSON.stringify(puzzle_input));
	exploreNodeP1(input, { id: "start", big: false }, []);
	return totalPathsP1.length;
}

function solvePartTwo(puzzle_input) {
	let input = JSON.parse(JSON.stringify(puzzle_input));
	exploreNodeP2(input, { id: "start", big: false }, []);
	return totalPathsP2.length;
}

function printAnswers(puzzle_input) {
	console.table({
		"Part 1 :": solvePartOne(puzzle_input),
		"Part 2 :": solvePartTwo(puzzle_input),
	});
}

parseInput("inputs/day12.txt");
