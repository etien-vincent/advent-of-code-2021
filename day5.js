let reader = require("line-reader");
function parseInput(path) {
	let input = [];
	reader.eachLine(path, function (line, last) {
		input.push({
			x1: parseInt(line.split(" -> ")[0].split(",")[0]),
			y1: parseInt(line.split(" -> ")[0].split(",")[1]),
			x2: parseInt(line.split(" -> ")[1].split(",")[0]),
			y2: parseInt(line.split(" -> ")[1].split(",")[1]),
		});
		if (last) {
			printAnswers(input);
		}
	});
}

function calculatePointList(p1x, p1y, p2x, p2y) {
	let list = [];
	if (p1x == p2x) {
		if (p1y >= p2y) {
			for (p2y; p2y <= p1y; p2y++) {
				list.push({ x: p1x, y: p2y });
			}
		} else {
			for (p1y; p1y <= p2y; p1y++) {
				list.push({ x: p1x, y: p1y });
			}
		}
	}
	if (p1y == p2y) {
		if (p1x >= p2x) {
			for (p2x; p2x <= p1x; p2x++) {
				list.push({ x: p2x, y: p2y });
			}
		} else {
			for (p1x; p1x <= p2x; p1x++) {
				list.push({ x: p1x, y: p1y });
			}
		}
	}
	if (p1y != p2y && p1x != p2x) {
		let xSign = (p2x - p1x) / Math.abs(p2x - p1x);
		let ySign = (p2y - p1y) / Math.abs(p2y - p1y);
		for (p1x = p1x; p1x != p2x; p1x += xSign) {
			list.push({ x: p1x, y: p1y });
			p1y += ySign;
		}
		list.push({ x: p2x, y: p2y });
	}
	return list;
}

function solvePartOne(input) {
	input = input.filter((s) => {
		return s.x1 === s.x2 || s.y1 === s.y2;
	});
	let pointsOnGraph = [];
	for (let i = 0; i < input.length; i++) {
		const s1 = input[i];
		pointsOnGraph = pointsOnGraph.concat(
			calculatePointList(s1.x1, s1.y1, s1.x2, s1.y2)
		);
	}
	let doublePointsOnGraph = [];
	for (let i = 0; i < pointsOnGraph.length; i++) {
		const p = pointsOnGraph[i];
		if (
			i !==
			pointsOnGraph.findIndex((point) => point.x == p.x && point.y == p.y)
		) {
			doublePointsOnGraph.push(p);
		}
	}
	doublePointsOnGraph = doublePointsOnGraph.filter(
		(p, i, arr) =>
			i === arr.findIndex((point) => point.x == p.x && point.y == p.y)
	);
	return doublePointsOnGraph.length;
}

function solvePartTwo(input) {
	let pointsOnGraph = [];
	for (let i = 0; i < input.length; i++) {
		const s1 = input[i];
		pointsOnGraph = pointsOnGraph.concat(
			calculatePointList(s1.x1, s1.y1, s1.x2, s1.y2)
		);
	}
	let doublePointsOnGraph = [];
	for (let i = 0; i < pointsOnGraph.length; i++) {
		const p = pointsOnGraph[i];
		if (
			i !==
			pointsOnGraph.findIndex((point) => point.x == p.x && point.y == p.y)
		) {
			doublePointsOnGraph.push(p);
		}
	}
	doublePointsOnGraph = doublePointsOnGraph.filter(
		(p, i, arr) =>
			i === arr.findIndex((point) => point.x == p.x && point.y == p.y)
	);
	return doublePointsOnGraph.length;
}

function printAnswers(input) {
	console.table({
		"Part 1 :": solvePartOne(input),
		"Part 2 :": solvePartTwo(input),
	});
}

parseInput("inputs/day5.txt");
