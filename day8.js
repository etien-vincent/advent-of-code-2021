let reader = require("line-reader");
function parseInput(path) {
	let input = [];
	let output = [];
	reader.eachLine(path, function (line, last) {
		input.push(line.split(" | ")[0].split(" "));
		output.push(line.split(" | ")[1].split(" "));
		if (last) {
			printAnswers(input, output);
		}
	});
}

function solvePartOne(input, output) {
	let count = 0;
	output.forEach((line) => {
		line.forEach((signal) => {
			if (
				signal.split("").length === 2 ||
				signal.split("").length === 4 ||
				signal.split("").length === 3 ||
				signal.split("").length === 7
			) {
				count++;
			}
		});
	});
	return count;
}

function solvePartTwo(input, output) {
	const possible_letters = "abcdefg".split("");
	let total = 0;
	for (let i = 0; i < input.length; i++) {
		let conversion_table = [0, 0, 0, 0, 0, 0, 0];
		let current_input = input[i];
		let current_output = output[i];
		current_input = current_input.sort((a, b) => a.length - b.length);
		// console.log(current_input);
		//add top segment to the conversion table
		for (let j = 0; j < current_input[1].split("").length; j++) {
			if (!current_input[0].includes(current_input[1].split("")[j])) {
				conversion_table[0] = current_input[1].split("")[j];
			}
		}
		//go through 6 length input to find the two right segments
		for (let j = 6; j < current_input.length - 1; j++) {
			const letters = current_input[j].split("");
			for (let k = 0; k < possible_letters.length; k++) {
				if (
					!letters.includes(possible_letters[k]) &&
					current_input[0].includes(possible_letters[k])
				) {
					conversion_table[2] = possible_letters[k];
					current_input[0].split("").forEach((letr) => {
						if (letr !== possible_letters[k]) {
							conversion_table[5] = letr;
						}
					});
				}
			}
		}
		for (let j = 0; j < possible_letters.length; j++) {
			let letter_count = 0;
			for (let k = 0; k < current_input.length; k++) {
				if (current_input[k].includes(possible_letters[j])) {
					letter_count++;
				}
			}
			if (letter_count === 4) {
				conversion_table[4] = possible_letters[j];
			}
			if (letter_count === 6) {
				conversion_table[1] = possible_letters[j];
			}
			if (
				letter_count === 7 &&
				current_input[2].includes(possible_letters[j])
			) {
				conversion_table[3] = possible_letters[j];
			}
		}
		for (let j = 0; j < possible_letters.length; j++) {
			if (!conversion_table.includes(possible_letters[j])) {
				conversion_table[6] = possible_letters[j];
			}
		}
		let outputNumber = "";
		for (let j = 0; j < current_output.length; j++) {
			let activated_seg = [0, 0, 0, 0, 0, 0, 0];
			for (let k = 0; k < current_output[j].split("").length; k++) {
				activated_seg[
					conversion_table.findIndex(
						(a) => a === current_output[j].split("")[k]
					)
				] = 1;
			}
			switch (activated_seg.toString()) {
				case [1, 1, 1, 0, 1, 1, 1].toString():
					outputNumber = outputNumber + "0";
					break;
				case [0, 0, 1, 0, 0, 1, 0].toString():
					outputNumber = outputNumber + "1";
					break;
				case [1, 0, 1, 1, 1, 0, 1].toString():
					outputNumber = outputNumber + "2";
					break;
				case [1, 0, 1, 1, 0, 1, 1].toString():
					outputNumber = outputNumber + "3";
					break;
				case [0, 1, 1, 1, 0, 1, 0].toString():
					outputNumber = outputNumber + "4";
					break;
				case [1, 1, 0, 1, 0, 1, 1].toString():
					outputNumber = outputNumber + "5";
					break;
				case [1, 1, 0, 1, 1, 1, 1].toString():
					outputNumber = outputNumber + "6";
					break;
				case [1, 0, 1, 0, 0, 1, 0].toString():
					outputNumber = outputNumber + "7";
					break;
				case [1, 1, 1, 1, 1, 1, 1].toString():
					outputNumber = outputNumber + "8";
					break;
				case [1, 1, 1, 1, 0, 1, 1].toString():
					outputNumber = outputNumber + "9";
					break;

				default:
					console.log("c la sauce");
					break;
			}
		}
		total += parseInt(outputNumber);
	}
	return total;
}

function printAnswers(input, output) {
	console.table({
		"Part 1 :": solvePartOne(input, output),
		"Part 2 :": solvePartTwo(input, output),
	});
}

parseInput("inputs/day8.txt");
