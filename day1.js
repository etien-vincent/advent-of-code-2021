let reader = require("line-reader");
function parseInput(path) {
	let input = [];
	reader.eachLine(path, function (line, last) {
		input.push(parseInt(line));
		if (last) {
			printAnswers(input);
		}
	});
}

function solvePartOne(input) {
	let lastvalue = input[0];
	let count = 0;
	input.forEach((element) => {
		if (element > lastvalue) {
			count++;
		}
		lastvalue = element;
	});
	return count;
}

function solvePartTwo(input) {
	let lastvalues = [input[0], input[1], input[2]];
	let count = 0;
	input.forEach((element, i) => {
		if (i > 2) {
			let oldSum = lastvalues.reduce((a, b) => a + b, 0);
			lastvalues.push(element);
			lastvalues.shift();
			let newSum = lastvalues.reduce((a, b) => a + b, 0);
			if (newSum > oldSum) {
				count++;
			}
		}
	});
	return count;
}

function printAnswers(input) {
	console.table({
		"Part 1 :": solvePartOne(input),
		"Part 2 :": solvePartTwo(input),
	});
}

parseInput("inputs/day1.txt");
