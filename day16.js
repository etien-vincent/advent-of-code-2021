let reader = require("line-reader");
function parseInput(path) {
	let puzzle_input = [];
	reader.eachLine(path, function (line, last) {
		puzzle_input = line;
		if (last) {
			printAnswers(puzzle_input);
		}
	});
}
const conversionTable = {
	0: [0, 0, 0, 0],
	1: [0, 0, 0, 1],
	2: [0, 0, 1, 0],
	3: [0, 0, 1, 1],
	4: [0, 1, 0, 0],
	5: [0, 1, 0, 1],
	6: [0, 1, 1, 0],
	7: [0, 1, 1, 1],
	8: [1, 0, 0, 0],
	9: [1, 0, 0, 1],
	A: [1, 0, 1, 0],
	B: [1, 0, 1, 1],
	C: [1, 1, 0, 0],
	D: [1, 1, 0, 1],
	E: [1, 1, 1, 0],
	F: [1, 1, 1, 1],
};
let globalTotal = 0;
function readBit(hexaArray, bitPosition) {
	if (hexaArray[Math.floor(bitPosition / 4)] == undefined) {
		return undefined;
	}
	const hexaValue = hexaArray[Math.floor(bitPosition / 4)];
	const bits = conversionTable[hexaValue];
	return bits[bitPosition % 4];
}

function readPacket(bitArray, startPos) {
	const packetVersion = parseInt(
		[
			bitArray[startPos],
			bitArray[startPos + 1],
			bitArray[startPos + 2],
		].join(""),
		2
	);
	const packetTypeId = parseInt(
		[
			bitArray[startPos + 3],
			bitArray[startPos + 4],
			bitArray[startPos + 5],
		].join(""),
		2
	);
	let curr_pos = startPos + 6;
	let value;
	if (packetTypeId === 4) {
		let lastDig = false;
		let number = [];
		while (!lastDig) {
			number.push(
				...[
					bitArray[curr_pos + 1],
					bitArray[curr_pos + 2],
					bitArray[curr_pos + 3],
					bitArray[curr_pos + 4],
				]
			);
			if (bitArray[curr_pos] === 0) {
				lastDig = true;
			}
			curr_pos += 5;
		}
		value = parseInt(number.join(""), 2);
	} else {
		let subPacketValues = [];
		if (bitArray[curr_pos] === 0) {
			curr_pos++;
			const nbBitsSub = parseInt(
				bitArray.slice(curr_pos, curr_pos + 15).join(""),
				2
			);
			curr_pos += 15;
			const endvalue = curr_pos + nbBitsSub;
			while (curr_pos < endvalue) {
				let infos = readPacket(bitArray, curr_pos);
				subPacketValues.push(infos.value);
				curr_pos = infos.endPos;
			}
		} else {
			curr_pos++;
			const nbPackets = parseInt(
				bitArray.slice(curr_pos, curr_pos + 11).join(""),
				2
			);
			curr_pos += 11;
			for (let packetnum = 0; packetnum < nbPackets; packetnum++) {
				let infos = readPacket(bitArray, curr_pos);
				subPacketValues.push(infos.value);
				curr_pos = infos.endPos;
			}
		}
		switch (packetTypeId) {
			case 0:
				value = subPacketValues.reduce((a, b) => a + b, 0);
				break;
			case 1:
				value = subPacketValues.reduce((a, b) => a * b, 1);
				break;
			case 2:
				value = Math.min(...subPacketValues);
				break;
			case 3:
				value = Math.max(...subPacketValues);
				break;
			case 5:
				value = subPacketValues[0] > subPacketValues[1] ? 1 : 0;
				break;
			case 6:
				value = subPacketValues[0] < subPacketValues[1] ? 1 : 0;
				break;
			case 7:
				value = subPacketValues[0] === subPacketValues[1] ? 1 : 0;
				break;

			default:
				break;
		}
	}
	globalTotal += packetVersion;
	return {
		packetVersion,
		packetTypeId,
		value,
		endPos: curr_pos,
		globalVersionTotal: globalTotal,
	};
}

function solvePartOne(puzzle_input) {
	let input = [];
	puzzle_input.split("").forEach((element) => {
		input.push(...conversionTable[element]);
	});
	return readPacket(input, 0).globalVersionTotal;
}

function solvePartTwo(puzzle_input) {
	let input = [];
	puzzle_input.split("").forEach((element) => {
		input.push(...conversionTable[element]);
	});
	return readPacket(input, 0).value;
}

function printAnswers(puzzle_input) {
	console.table({
		"Part 1 :": solvePartOne(puzzle_input),
		"Part 2 :": solvePartTwo(puzzle_input),
	});
}

parseInput("inputs/day16.txt");
