let reader = require("line-reader");
function parseInput(path) {
	let puzzle_input = [];
	reader.eachLine(path, function (line, last) {
		puzzle_input.push(line.split("").map((el) => parseInt(el)));
		if (last) {
			printAnswers(puzzle_input);
		}
	});
}

function solvePartOne(puzzle_input) {
	let input = puzzle_input.map((line) => line.map((a) => a));
	const noFlash = input.map((line) => line.map((el) => 0));
	let flashed = [...noFlash];
	let flashcount = 0;
	for (let i = 0; i < 100; i++) {
		for (let j = 0; j < input.length; j++) {
			for (let k = 0; k < input[j].length; k++) {
				input[j][k]++;
			}
		}
		let flashedThisSubStep = true;
		while (flashedThisSubStep) {
			flashedThisSubStep = false;
			for (let j = 0; j < input.length; j++) {
				for (let k = 0; k < input[j].length; k++) {
					if (input[j][k] > 9 && flashed[j][k] === 0) {
						flashed[j][k] = 1;
						flashedThisSubStep = true;
						if (input[j - 1] !== undefined) {
							input[j - 1][k]++;
							if (input[j - 1][k - 1] !== undefined) {
								input[j - 1][k - 1]++;
							}
							if (input[j - 1][k + 1] !== undefined) {
								input[j - 1][k + 1]++;
							}
						}
						if (input[j + 1] !== undefined) {
							input[j + 1][k]++;
							if (input[j + 1][k - 1] !== undefined) {
								input[j + 1][k - 1]++;
							}
							if (input[j + 1][k + 1] !== undefined) {
								input[j + 1][k + 1]++;
							}
						}
						if (input[j][k - 1] !== undefined) {
							input[j][k - 1]++;
						}
						if (input[j][k + 1] !== undefined) {
							input[j][k + 1]++;
						}
					}
				}
			}
		}
		for (let j = 0; j < flashed.length; j++) {
			for (let k = 0; k < flashed[j].length; k++) {
				if (flashed[j][k] === 1) {
					input[j][k] = 0;
					flashcount++;
				}
			}
		}
		flashed = input.map((line) => line.map((el) => 0));
	}
	return flashcount;
}

function solvePartTwo(puzzle_input) {
	let input = puzzle_input.map((line) => line.map((a) => a));
	const noFlash = input.map((line) => line.map((el) => 0));
	let flashed = [...noFlash];
	let flashcount = 0;
	for (let i = 0; i < 10000; i++) {
		for (let j = 0; j < input.length; j++) {
			for (let k = 0; k < input[j].length; k++) {
				input[j][k]++;
			}
		}
		let flashedThisSubStep = true;
		while (flashedThisSubStep) {
			flashedThisSubStep = false;
			for (let j = 0; j < input.length; j++) {
				for (let k = 0; k < input[j].length; k++) {
					if (input[j][k] > 9 && flashed[j][k] === 0) {
						flashed[j][k] = 1;
						flashedThisSubStep = true;
						if (input[j - 1] !== undefined) {
							input[j - 1][k]++;
							if (input[j - 1][k - 1] !== undefined) {
								input[j - 1][k - 1]++;
							}
							if (input[j - 1][k + 1] !== undefined) {
								input[j - 1][k + 1]++;
							}
						}
						if (input[j + 1] !== undefined) {
							input[j + 1][k]++;
							if (input[j + 1][k - 1] !== undefined) {
								input[j + 1][k - 1]++;
							}
							if (input[j + 1][k + 1] !== undefined) {
								input[j + 1][k + 1]++;
							}
						}
						if (input[j][k - 1] !== undefined) {
							input[j][k - 1]++;
						}
						if (input[j][k + 1] !== undefined) {
							input[j][k + 1]++;
						}
					}
				}
			}
		}
		let currFlashes = 0;
		for (let j = 0; j < flashed.length; j++) {
			for (let k = 0; k < flashed[j].length; k++) {
				if (flashed[j][k] === 1) {
					input[j][k] = 0;
					flashcount++;
					currFlashes++;
				}
			}
		}
		if (currFlashes == 100) {
			return i + 1;
		}
		flashed = input.map((line) => line.map((el) => 0));
	}
	return flashcount;
}

function printAnswers(puzzle_input) {
	console.table({
		"Part 1 :": solvePartOne(puzzle_input),
		"Part 2 :": solvePartTwo(puzzle_input),
	});
}

parseInput("inputs/day11.txt");
