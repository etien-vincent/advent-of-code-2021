let reader = require("line-reader");
function parseInput(path) {
	let input = [];
	reader.eachLine(path, function (line, last) {
		let [direction, value] = line.split(" ");
		input.push([direction, parseInt(value)]);
		if (last) {
			printAnswers(input);
		}
	});
}

function solvePartOne(input) {
	let depth = 0;
	let distance = 0;
	input.forEach((movement) => {
		switch (movement[0]) {
			case "forward":
				distance += movement[1];
				break;
			case "down":
				depth += movement[1];
				break;
			case "up":
				depth -= movement[1];
				break;

			default:
				console.log("error");
				break;
		}
	});
	return depth * distance;
}

function solvePartTwo(input) {
	let depth = 0;
	let distance = 0;
	let aim = 0;
	input.forEach((movement) => {
		switch (movement[0]) {
			case "forward":
				distance += movement[1];
				depth += aim * movement[1];
				break;
			case "down":
				aim += movement[1];
				break;
			case "up":
				aim -= movement[1];
				break;

			default:
				console.log("error");
				break;
		}
	});
	return depth * distance;
}

function printAnswers(input) {
	console.table({
		"Part 1 :": solvePartOne(input),
		"Part 2 :": solvePartTwo(input),
	});
}

parseInput("inputs/day2.txt");
