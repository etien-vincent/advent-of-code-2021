let reader = require("line-reader");
function parseInput(path) {
	let puzzle_input = [];
	reader.eachLine(path, function (line, last) {
		let [turnedOn, rest] = line.split(" ");
		let [xinfo, yinfo, zinfo] = rest.split(",");
		puzzle_input.unshift({
			on: turnedOn === "on" ? true : false,
			x1: parseInt(xinfo.split("x=")[1].split("..")[0]),
			x2: parseInt(xinfo.split("x=")[1].split("..")[1]),
			y1: parseInt(yinfo.split("y=")[1].split("..")[0]),
			y2: parseInt(yinfo.split("y=")[1].split("..")[1]),
			z1: parseInt(zinfo.split("z=")[1].split("..")[0]),
			z2: parseInt(zinfo.split("z=")[1].split("..")[1]),
		});
		if (last) {
			printAnswers(puzzle_input);
		}
	});
}

function solvePartOne(puzzle_input) {
	let count = 0;
	for (let i = -50; i < 51; i++) {
		for (let j = -50; j < 51; j++) {
			for (let k = -50; k < 51; k++) {
				for (let index = 0; index < puzzle_input.length; index++) {
					if (
						i >= puzzle_input[index].x1 &&
						i <= puzzle_input[index].x2 &&
						j >= puzzle_input[index].y1 &&
						j <= puzzle_input[index].y2 &&
						k >= puzzle_input[index].z1 &&
						k <= puzzle_input[index].z2
					) {
						if (puzzle_input[index].on) {
							count++;
						}
						break;
					}
				}
			}
		}
	}
	return count;
}

function solvePartTwo(puzzle_input) {
	function comBinedRectangle(rec1, rec2) {
		let overlappedRec = JSON.parse(JSON.stringify(rec1));
		const NolappingFromX = rec1.x2 < rec2.x1 || rec1.x1 > rec2.x2;
		const NolappingFromY = rec1.y2 < rec2.y1 || rec1.y1 > rec2.y2;
		const NolappingFromZ = rec1.z2 < rec2.z1 || rec1.z1 > rec2.z2;
		if (!(NolappingFromX || NolappingFromY || NolappingFromZ)) {
			overlappedRec.x1 = Math.max(rec1.x1, rec2.x1);
			overlappedRec.y1 = Math.max(rec1.y1, rec2.y1);
			overlappedRec.x2 = Math.min(rec1.x2, rec2.x2);
			overlappedRec.y2 = Math.min(rec1.y2, rec2.y2);
			overlappedRec.z1 = Math.max(rec1.z1, rec2.z1);
			overlappedRec.z2 = Math.min(rec1.z2, rec2.z2);
			return overlappedRec;
		}
	}
	function unionArea(array) {
		let area = 0;
		for (const rect of array) {
			area +=
				(rect.x2 - rect.x1 + 1) *
				(rect.y2 - rect.y1 + 1) *
				(rect.z2 - rect.z1 + 1);
			let intersections = [];
			for (let j = 0; j < array.length; j++) {
				let intersect = comBinedRectangle(rect, array[j]);
				if (intersect != undefined) {
					intersections.push(intersect);
				}
			}
			intersections = intersections.filter(
				(value, index, self) =>
					index ===
					self.findIndex(
						(t) =>
							t.x1 === value.x1 &&
							t.y1 === value.y1 &&
							t.z1 === value.z1 &&
							t.x2 === value.x2 &&
							t.y2 === value.y2 &&
							t.z2 === value.z2
					)
			);
			if (intersections.length !== 0) {
				area -= unionArea(intersections);
			}
		}
		return area;
	}

	let input = JSON.parse(JSON.stringify(puzzle_input));
	let count = 0;
	let zones = [];
	for (const zone of input) {
		if (zone.on) {
			let monAire =
				(zone.x2 - zone.x1 + 1) *
				(zone.y2 - zone.y1 + 1) *
				(zone.z2 - zone.z1 + 1);
			count += monAire;
			let intersections = [];
			for (let j = 0; j < zones.length; j++) {
				let intersect = comBinedRectangle(zone, zones[j]);
				if (intersect != undefined) {
					intersections.push(intersect);
				}
			}
			intersections = intersections.filter(
				(value, index, self) =>
					index ===
					self.findIndex(
						(t) =>
							t.x1 === value.x1 &&
							t.y1 === value.y1 &&
							t.z1 === value.z1 &&
							t.x2 === value.x2 &&
							t.y2 === value.y2 &&
							t.z2 === value.z2
					)
			);
			count -= unionArea(intersections);
		}
		if (zone != undefined) {
			zones.push(zone);
		}
	}
	return count;
}

function printAnswers(puzzle_input) {
	console.table({
		"Part 1 :": solvePartOne(puzzle_input),
		"Part 2 :": solvePartTwo(puzzle_input),
	});
}

parseInput("inputs/day22.txt");
