let reader = require("line-reader");
function parseInput(path) {
	let input = [0, 0, 0, 0, 0, 0, 0, 0, 0];
	reader.eachLine(path, function (line, last) {
		line.split(",").forEach((el) => {
			input[parseInt(el)] = input[parseInt(el)] + 1;
		});
		if (last) {
			printAnswers(input);
		}
	});
}
function runSimulation(observation, nbDays) {
	for (let day = 0; day < nbDays; day++) {
		const newInput = [
			observation[1],
			observation[2],
			observation[3],
			observation[4],
			observation[5],
			observation[6],
			observation[7] + observation[0],
			observation[8],
			observation[0],
		];
		observation = newInput;
	}
	return observation;
}

function solvePartOne(input) {
	return runSimulation(input, 80).reduce((a, b) => a + b, 0);
}

function solvePartTwo(input) {
	return runSimulation(input, 256).reduce((a, b) => a + b, 0);
}

function printAnswers(input) {
	console.table({
		"Part 1 :": solvePartOne(input),
		"Part 2 :": solvePartTwo(input),
	});
}

parseInput("inputs/day6.txt");
