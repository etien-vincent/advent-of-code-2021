function parseInput() {
	printAnswers({ xmin: 139, xmax: 187, ymin: -148, ymax: -89 });
}

function solvePartOne(puzzle_input) {
	let realmax = -Infinity;
	for (let vy = puzzle_input.ymin; vy < 160; vy++) {
		for (let vx = 0; vx <= puzzle_input.xmax; vx++) {
			let dx = 0;
			let dy = 0;
			let t = 0;
			let vx_curr = vx;
			let vy_curr = vy;
			let localmax = -Infinity;
			while (
				dx <= puzzle_input.xmax + 40 &&
				dy >= puzzle_input.ymin - 40
			) {
				dx += vx_curr;
				dy += vy_curr;
				vy_curr--;
				if (vx_curr > 0) {
					vx_curr--;
				} else if (vx_curr < 0) {
					vx_curr++;
				}
				t++;
				if (dy > localmax) {
					localmax = dy;
				}
				if (
					dx >= puzzle_input.xmin &&
					dx <= puzzle_input.xmax &&
					dy >= puzzle_input.ymin &&
					dy <= puzzle_input.ymax
				) {
					if (localmax > realmax) {
						realmax = localmax;
					}
					break;
				}
			}
		}
	}
	return realmax;
}

function solvePartTwo(puzzle_input) {
	let count = 0;
	for (let vy = puzzle_input.ymin; vy < 160; vy++) {
		for (let vx = 0; vx <= puzzle_input.xmax; vx++) {
			let dx = 0;
			let dy = 0;
			let t = 0;
			let vx_curr = vx;
			let vy_curr = vy;
			let localmax = -Infinity;
			while (
				dx <= puzzle_input.xmax + 40 &&
				dy >= puzzle_input.ymin - 40
			) {
				dx += vx_curr;
				dy += vy_curr;
				vy_curr--;
				if (vx_curr > 0) {
					vx_curr--;
				} else if (vx_curr < 0) {
					vx_curr++;
				}
				t++;
				if (dy > localmax) {
					localmax = dy;
				}
				if (
					dx >= puzzle_input.xmin &&
					dx <= puzzle_input.xmax &&
					dy >= puzzle_input.ymin &&
					dy <= puzzle_input.ymax
				) {
					count++;
					break;
				}
			}
		}
	}
	return count;
}

function printAnswers(puzzle_input) {
	console.table({
		"Part 1 :": solvePartOne(puzzle_input),
		"Part 2 :": solvePartTwo(puzzle_input),
	});
}

parseInput();
