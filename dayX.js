let reader = require("line-reader");
function parseInput(path) {
	let puzzle_input = [];
	reader.eachLine(path, function (line, last) {
		puzzle_input.push(line);
		if (last) {
			printAnswers(puzzle_input);
		}
	});
}

function solvePartOne(puzzle_input) {
	console.log(puzzle_input);
	return 0;
}

function solvePartTwo(puzzle_input) {
	return 0;
}

function printAnswers(puzzle_input) {
	console.table({
		"Part 1 :": solvePartOne(puzzle_input),
		"Part 2 :": solvePartTwo(puzzle_input),
	});
}

parseInput("inputs/dayX.txt");
