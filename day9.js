let reader = require("line-reader");
let output = require("image-output");

function parseInput(path) {
	let input = [];
	reader.eachLine(path, function (line, last) {
		input.push(line.split("").map((el) => parseInt(el)));
		if (last) {
			printAnswers(input);
		}
	});
}

function solvePartOne(input) {
	let sum = 0;
	for (let i = 0; i < input.length; i++) {
		for (let j = 0; j < input[i].length; j++) {
			let isLowPoint = true;
			if (input[i - 1] != undefined && input[i - 1][j] <= input[i][j]) {
				isLowPoint = false;
			}
			if (input[i + 1] != undefined && input[i + 1][j] <= input[i][j]) {
				isLowPoint = false;
			}
			if (
				input[i][j - 1] != undefined &&
				input[i][j - 1] <= input[i][j]
			) {
				isLowPoint = false;
			}
			if (
				input[i][j + 1] != undefined &&
				input[i][j + 1] <= input[i][j]
			) {
				isLowPoint = false;
			}
			if (isLowPoint) {
				sum += input[i][j] + 1;
			}
		}
	}
	return sum;
}

function solvePartTwo(input) {
	let k = 0;
	let bassinId = -1;
	let bassinMap = [...input];
	for (let i = 0; i < input.length; i++) {
		for (let j = 0; j < input[i].length; j++) {
			let isLowPoint = true;
			if (input[i - 1] != undefined && input[i - 1][j] <= input[i][j]) {
				isLowPoint = false;
			}
			if (input[i + 1] != undefined && input[i + 1][j] <= input[i][j]) {
				isLowPoint = false;
			}
			if (
				input[i][j - 1] != undefined &&
				input[i][j - 1] <= input[i][j]
			) {
				isLowPoint = false;
			}
			if (
				input[i][j + 1] != undefined &&
				input[i][j + 1] <= input[i][j]
			) {
				isLowPoint = false;
			}
			if (isLowPoint) {
				bassinMap[i][j] = bassinId;
				bassinId--;
				saveImage(bassinMap, ++k);
			}
		}
	}
	let isChange = true;
	while (isChange) {
		isChange = false;
		let newBassinMap = [...bassinMap];
		for (let i = 0; i < input.length; i++) {
			for (let j = 0; j < input[i].length; j++) {
				if (bassinMap[i][j] >= 0 && bassinMap[i][j] !== 9) {
					let iDirection;
					let jDirection;
					if (
						bassinMap[i - 1] != undefined &&
						bassinMap[i - 1][j] < 0
					) {
						iDirection = i - 1;
						jDirection = j;
					}
					if (
						bassinMap[i + 1] != undefined &&
						bassinMap[i + 1][j] < 0
					) {
						if (
							jDirection == undefined ||
							(jDirection != undefined &&
								input[i + 1][j] < input[iDirection][jDirection])
						) {
							iDirection = i + 1;
							jDirection = j;
						}
					}
					if (
						bassinMap[i][j - 1] != undefined &&
						bassinMap[i][j - 1] < 0
					) {
						if (
							jDirection == undefined ||
							(jDirection != undefined &&
								input[i][j - 1] < input[iDirection][jDirection])
						) {
							iDirection = i;
							jDirection = j - 1;
						}
					}
					if (
						bassinMap[i][j + 1] != undefined &&
						bassinMap[i][j + 1] < 0
					) {
						if (
							jDirection == undefined ||
							(jDirection != undefined &&
								input[i][j + 1] < input[iDirection][jDirection])
						) {
							iDirection = i;
							jDirection = j + 1;
						}
					}
					if (jDirection != undefined) {
						isChange = true;
						newBassinMap[i][j] = bassinMap[iDirection][jDirection];
						saveImage(newBassinMap, ++k);
					}
				}
			}
		}
		saveImage(bassinMap, ++k);
		bassinMap = newBassinMap;
	}
	saveImage(bassinMap, "final");
	let dic = {};
	for (let i = 0; i < bassinMap.length; i++) {
		for (let j = 0; j < bassinMap[i].length; j++) {
			bassinMap[i][j];
			if (dic[bassinMap[i][j]] == undefined) {
				dic[bassinMap[i][j]] = 0;
			}
			dic[bassinMap[i][j]] += 1;
		}
	}
	dic[9] = 0;
	let bassinSizes = [];
	for (const key in dic) {
		bassinSizes.push(dic[key]);
	}
	bassinSizes.sort((a, b) => b - a);
	return bassinSizes[0] * bassinSizes[1] * bassinSizes[2];
}

function saveImage(arr, name) {
	arr = arr.map((el) => el.map((e) => [e < 0 ? 255 : 0, 0, 0]));
	// output(
	// 	{ data: arr, width: arr.length, height: arr.length },
	// 	`visualisations/day9-${name}.png`
	// );
}

function printAnswers(input) {
	console.table({
		"Part 1 :": solvePartOne(input),
		"Part 2 :": solvePartTwo(input),
	});
}

parseInput("inputs/day9.txt");
