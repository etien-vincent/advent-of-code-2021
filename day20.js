let reader = require("line-reader");
let output = require("image-output");

function parseInput(path) {
	let puzzle_input = [];
	let algorithm = "";
	reader.eachLine(path, function (line, last) {
		if (algorithm === "") {
			algorithm = line.split("").map((el) => {
				if (el === "#") {
					return 1;
				} else {
					return 0;
				}
			});
		} else if (line != "") {
			puzzle_input.push(
				line.split("").map((el) => {
					if (el === "#") {
						return 1;
					} else {
						return 0;
					}
				})
			);
		}
		if (last) {
			printAnswers(puzzle_input, algorithm);
		}
	});
}

function saveImage(arr, name) {
	arr = arr.map((el) => el.map((e) => [e == 1 ? 255 : 0, 0, 0]));
	// output(
	// 	{ data: arr, width: arr.length, height: arr.length },
	// 	`visualisations/day20-${name}.png`
	// );
}

function calcMediumValue(x, y, array, iteration_number, algorithm) {
	let binaryRes = [];
	if (array[x - 1] !== undefined) {
		if (array[x - 1][y - 1] !== undefined) {
			binaryRes.push(array[x - 1][y - 1]);
		} else {
			binaryRes.push(iteration_number);
		}
		binaryRes.push(array[x - 1][y]);
		if (array[x - 1][y + 1] !== undefined) {
			binaryRes.push(array[x - 1][y + 1]);
		} else {
			binaryRes.push(iteration_number);
		}
	} else {
		binaryRes.push(iteration_number, iteration_number, iteration_number);
	}
	if (array[x][y - 1] !== undefined) {
		binaryRes.push(array[x][y - 1]);
	} else {
		binaryRes.push(iteration_number);
	}
	binaryRes.push(array[x][y]);
	if (array[x][y + 1] !== undefined) {
		binaryRes.push(array[x][y + 1]);
	} else {
		binaryRes.push(iteration_number);
	}
	if (array[x + 1] !== undefined) {
		if (array[x + 1][y - 1] !== undefined) {
			binaryRes.push(array[x + 1][y - 1]);
		} else {
			binaryRes.push(iteration_number);
		}
		binaryRes.push(array[x + 1][y]);
		if (array[x + 1][y + 1] !== undefined) {
			binaryRes.push(array[x + 1][y + 1]);
		} else {
			binaryRes.push(iteration_number);
		}
	} else {
		binaryRes.push(iteration_number, iteration_number, iteration_number);
	}
	return algorithm[parseInt(binaryRes.join(""), 2)];
}

function expandImg(inputImg, filler) {
	let outputImg = [];
	for (let i = 0; i < 2; i++) {
		outputImg.push([...new Array(inputImg.length + 4).fill(filler)]);
	}
	for (let i = 0; i < inputImg.length; i++) {
		outputImg.push(
			[filler, filler].concat(inputImg[i].concat([filler, filler]))
		);
	}
	for (let i = 0; i < 2; i++) {
		outputImg.push([...new Array(inputImg.length + 4).fill(filler)]);
	}
	return outputImg;
}

function solvePartOne(puzzle_input, algorithm) {
	let array = JSON.parse(JSON.stringify(puzzle_input));
	for (let k = 0; k < 2; k++) {
		saveImage(array, "p1-" + k);
		let newArray = JSON.parse(JSON.stringify(array));
		for (let i = 0; i < array.length; i++) {
			for (let j = 0; j < array[i].length; j++) {
				newArray[i][j] = calcMediumValue(i, j, array, k % 2, algorithm);
			}
		}
		array = JSON.parse(JSON.stringify(newArray));
	}
	saveImage(array, "p1-2");
	let count = 0;
	for (let i = 0; i < array.length; i++) {
		for (let j = 0; j < array[i].length; j++) {
			if (array[i][j] === 1) {
				count++;
			}
		}
	}
	return count;
}

function solvePartTwo(puzzle_input, algorithm) {
	let array = JSON.parse(JSON.stringify(puzzle_input));

	for (let k = 0; k < 50; k++) {
		saveImage(array, k);
		let newArray = JSON.parse(JSON.stringify(array));
		for (let i = 0; i < array.length; i++) {
			for (let j = 0; j < array[i].length; j++) {
				newArray[i][j] = calcMediumValue(i, j, array, k % 2, algorithm);
			}
		}
		array = JSON.parse(JSON.stringify(newArray));
	}
	saveImage(array, 50);
	let count = 0;
	for (let i = 0; i < array.length; i++) {
		for (let j = 0; j < array[i].length; j++) {
			if (array[i][j] === 1) {
				count++;
			}
		}
	}
	return count;
}

function printAnswers(puzzle_input, algorithm) {
	console.table({
		"Part 1 :": solvePartOne(puzzle_input, algorithm),
		"Part 2 :": solvePartTwo(puzzle_input, algorithm),
	});
}

parseInput("inputs/day20.txt");
