let reader = require("line-reader");
function parseInput(path) {
	let puzzle_input = [];
	let scannerIndex = -1;
	reader.eachLine(path, function (line, last) {
		if (line !== "") {
			if (line.slice(0, 2) == "--") {
				scannerIndex++;
				puzzle_input[scannerIndex] = [];
			} else {
				puzzle_input[scannerIndex].push({
					x: parseInt(line.split(",")[0]),
					y: parseInt(line.split(",")[1]),
					z: parseInt(line.split(",")[2]),
				});
			}
		}

		if (last) {
			printAnswers(puzzle_input);
		}
	});
}
let scannersPositions = [{ x: 0, y: 0, z: 0 }];
function solvePartOne(puzzle_input) {
	let input = JSON.parse(JSON.stringify(puzzle_input));
	let allConfigs = input.map((el) => {
		return {
			config1: el,
			config2: el.map((coord) => {
				return { x: coord.x, y: -coord.z, z: coord.y };
			}),
			config3: el.map((coord) => {
				return { x: coord.x, y: -coord.y, z: -coord.z };
			}),
			config4: el.map((coord) => {
				return { x: coord.x, y: coord.z, z: -coord.y };
			}),
			config5: el.map((coord) => {
				return { x: -coord.y, y: coord.x, z: coord.z };
			}),
			config6: el.map((coord) => {
				return { x: coord.z, y: coord.x, z: coord.y };
			}),
			config7: el.map((coord) => {
				return { x: coord.y, y: coord.x, z: -coord.z };
			}),
			config8: el.map((coord) => {
				return { x: -coord.z, y: coord.x, z: -coord.y };
			}),
			config9: el.map((coord) => {
				return { x: -coord.x, y: -coord.y, z: coord.z };
			}),
			config10: el.map((coord) => {
				return { x: -coord.x, y: -coord.z, z: -coord.y };
			}),
			config11: el.map((coord) => {
				return { x: -coord.x, y: coord.y, z: -coord.z };
			}),
			config12: el.map((coord) => {
				return { x: -coord.x, y: coord.z, z: coord.y };
			}),
			config13: el.map((coord) => {
				return { x: coord.y, y: -coord.x, z: coord.z };
			}),
			config14: el.map((coord) => {
				return { x: coord.z, y: -coord.x, z: -coord.y };
			}),
			config15: el.map((coord) => {
				return { x: -coord.y, y: -coord.x, z: -coord.z };
			}),
			config16: el.map((coord) => {
				return { x: -coord.z, y: -coord.x, z: coord.y };
			}),
			config17: el.map((coord) => {
				return { x: -coord.z, y: coord.y, z: coord.x };
			}),
			config18: el.map((coord) => {
				return { x: coord.y, y: coord.z, z: coord.x };
			}),
			config19: el.map((coord) => {
				return { x: coord.z, y: -coord.y, z: coord.x };
			}),
			config20: el.map((coord) => {
				return { x: -coord.y, y: -coord.z, z: coord.x };
			}),
			config21: el.map((coord) => {
				return { x: -coord.z, y: -coord.y, z: -coord.x };
			}),
			config22: el.map((coord) => {
				return { x: -coord.y, y: coord.z, z: -coord.x };
			}),
			config23: el.map((coord) => {
				return { x: coord.z, y: coord.y, z: -coord.x };
			}),
			config24: el.map((coord) => {
				return { x: coord.y, y: -coord.z, z: -coord.x };
			}),
		};
	});
	let selectedPoints = allConfigs[0]["config1"];
	let selectedScanners = [0];
	let compPointIndex = 0;
	while (selectedScanners.length < allConfigs.length) {
		compPoint = selectedPoints[compPointIndex];
		for (let i = 0; i < allConfigs.length; i++) {
			if (!selectedScanners.includes(i)) {
				for (const config in allConfigs[i]) {
					if (Object.hasOwnProperty.call(allConfigs[i], config)) {
						for (let j = 0; j < allConfigs[i][config].length; j++) {
							let distx =
								compPoint.x - allConfigs[i][config][j].x;
							let disty =
								compPoint.y - allConfigs[i][config][j].y;
							let distz =
								compPoint.z - allConfigs[i][config][j].z;
							let lineUpCount = 0;
							for (
								let k = 0;
								k < allConfigs[i][config].length;
								k++
							) {
								for (
									let l = 0;
									l < selectedPoints.length;
									l++
								) {
									if (
										selectedPoints[l].x ===
											allConfigs[i][config][k].x +
												distx &&
										selectedPoints[l].y ===
											allConfigs[i][config][k].y +
												disty &&
										selectedPoints[l].z ===
											allConfigs[i][config][k].z + distz
									) {
										lineUpCount++;
										if (lineUpCount >= 12) {
											break;
										}
									}
								}
								if (lineUpCount >= 12) {
									break;
								}
							}
							if (lineUpCount >= 12) {
								selectedScanners.push(i);
								selectedPoints = selectedPoints.concat(
									allConfigs[i][config].map((point) => {
										return {
											x: point.x + distx,
											y: point.y + disty,
											z: point.z + distz,
										};
									})
								);
								scannersPositions.push({
									x: distx,
									y: disty,
									z: distz,
								});
								selectedPoints = selectedPoints.filter(
									(value, index, self) =>
										index ===
										self.findIndex(
											(t) =>
												t.x === value.x &&
												t.y === value.y &&
												t.z === value.z
										)
								);
							}
						}
					}
				}
			}
		}
		compPointIndex++;
		if (compPointIndex == selectedPoints.length) {
			compPointIndex = 0;
		}
	}
	return selectedPoints.length;
}

function solvePartTwo(puzzle_input) {
	let max = -Infinity;
	for (let i = 0; i < scannersPositions.length; i++) {
		for (let j = 0; j < scannersPositions.length; j++) {
			let dist =
				Math.abs(scannersPositions[i].x - scannersPositions[j].x) +
				Math.abs(scannersPositions[i].y - scannersPositions[j].y) +
				Math.abs(scannersPositions[i].z - scannersPositions[j].z);
			if (dist > max) {
				max = dist;
			}
		}
	}
	return max;
}

function printAnswers(puzzle_input) {
	console.table({
		"Part 1 :": solvePartOne(puzzle_input),
		"Part 2 :": solvePartTwo(puzzle_input),
	});
}

parseInput("inputs/day19.txt");
