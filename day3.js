let reader = require("line-reader");
function parseInput(path) {
	let input = [];
	reader.eachLine(path, function (line, last) {
		input.push(line);
		if (last) {
			printAnswers(input);
		}
	});
}

function solvePartOne(input) {
	let bitCountArray = [];
	input.forEach((binaryNumber) => {
		binaryNumber.split("").forEach((bit, index) => {
			if (bitCountArray[index]) {
				bitCountArray[index] += parseInt(bit);
			} else {
				bitCountArray[index] = parseInt(bit);
			}
		});
	});
	let gammarate = parseInt(
		bitCountArray
			.map((el) => {
				if (el > input.length / 2) {
					return "1";
				} else {
					return "0";
				}
			})
			.join(""),
		2
	);
	let epsilonrate = parseInt(
		bitCountArray
			.map((el) => {
				if (el < input.length / 2) {
					return "1";
				} else {
					return "0";
				}
			})
			.join(""),
		2
	);
	return gammarate * epsilonrate;
}

function solvePartTwo(input) {
	let bitCountArray = [];
	input.forEach((binaryNumber) => {
		binaryNumber.split("").forEach((bit, index) => {
			if (bitCountArray[index]) {
				bitCountArray[index] += parseInt(bit);
			} else {
				bitCountArray[index] = parseInt(bit);
			}
		});
	});
	bitCountArray = bitCountArray.map((el) => {
		if (el > input.length / 2) {
			return "1";
		} else {
			return "0";
		}
	});
	let oxGenInput = [...input];
	let co2ScrubInput = [...input];
	let oxGenRating;
	let co2ScrubRating;
	bitCountArray.forEach((bit, index) => {
		let oxGenBit = 0;
		oxGenInput.forEach((line) => {
			oxGenBit += parseInt(line.split("")[index]);
		});
		if (oxGenBit >= oxGenInput.length / 2) {
			oxGenBit = "1";
		} else {
			oxGenBit = "0";
		}
		let co2ScrubBit = 0;
		co2ScrubInput.forEach((line) => {
			co2ScrubBit += parseInt(line.split("")[index]);
		});
		if (co2ScrubBit >= co2ScrubInput.length / 2) {
			co2ScrubBit = "0";
		} else {
			co2ScrubBit = "1";
		}
		oxGenInput = oxGenInput.filter((line) => {
			return line.split("")[index] === oxGenBit;
		});
		co2ScrubInput = co2ScrubInput.filter((line) => {
			return line.split("")[index] === co2ScrubBit;
		});
		if (oxGenInput.length === 1) {
			oxGenRating = parseInt(oxGenInput[0], 2);
			console.log(co2ScrubInput, oxGenInput);
		}
		if (co2ScrubInput.length === 1) {
			co2ScrubRating = parseInt(co2ScrubInput[0], 2);
			console.log(co2ScrubInput, oxGenInput);
		}
	});

	return co2ScrubRating * oxGenRating;
}

function printAnswers(input) {
	console.table({
		"Part 1 :": solvePartOne(input),
		"Part 2 :": solvePartTwo(input),
	});
}

parseInput("inputs/day3.txt");
