let reader = require("line-reader");
function parseInput(path) {
	let puzzle_input = [];
	reader.eachLine(path, function (line, last) {
		puzzle_input.push(parseInt(line.split("starting position: ")[1]));
		if (last) {
			printAnswers(puzzle_input);
		}
	});
}

function solvePartOne(puzzle_input) {
	let input = [puzzle_input[0] - 1, puzzle_input[1] - 1];
	let diceValue = 1;
	let dicethrowcount = 0;
	let scores = [0, 0];
	let p1turn = true;
	while (scores[0] < 1000 && scores[1] < 1000) {
		if (p1turn) {
			for (let i = 0; i < 3; i++) {
				input[0] = (input[0] + diceValue) % 10;
				diceValue++;
				dicethrowcount++;
				if (diceValue == 101) {
					diceValue = 1;
				}
			}
			scores[0] += input[0] + 1;
		} else {
			for (let i = 0; i < 3; i++) {
				input[1] = (input[1] + diceValue) % 10;
				diceValue++;
				dicethrowcount++;

				if (diceValue == 101) {
					diceValue = 1;
				}
			}
			scores[1] += input[1] + 1;
		}
		p1turn = !p1turn;
	}
	return scores[0] >= 1000
		? dicethrowcount * scores[1]
		: dicethrowcount * scores[0];
}

function solvePartTwo(puzzle_input) {
	let universesByTripleThrow = [0, 0, 0, 1, 3, 6, 7, 6, 3, 1];
	let mastringDuSwag = `${puzzle_input[0] - 1},${puzzle_input[1] - 1},0,0`;
	let universes = {};
	universes[mastringDuSwag] = 1;
	let p1wins = 0;
	let p2wins = 0;
	let p1turn = true;
	while (Object.keys(universes).length !== 0) {
		let newUniv = {};
		for (const univType in universes) {
			if (Object.hasOwnProperty.call(universes, univType)) {
				let curUnivN = parseInt(universes[univType]);
				let posP1 = parseInt(univType.split(",")[0]);
				let posP2 = parseInt(univType.split(",")[1]);
				let scoreP1 = parseInt(univType.split(",")[2]);
				let scoreP2 = parseInt(univType.split(",")[3]);
				if (p1turn) {
					for (let diceRoll = 3; diceRoll < 10; diceRoll++) {
						if (scoreP1 + ((posP1 + diceRoll) % 10) + 1 >= 21) {
							p1wins +=
								curUnivN * universesByTripleThrow[diceRoll];
						} else {
							if (
								newUniv[
									[
										(posP1 + diceRoll) % 10,
										posP2,
										scoreP1 + ((posP1 + diceRoll) % 10) + 1,
										scoreP2,
									].join(",")
								] == undefined
							) {
								newUniv[
									[
										(posP1 + diceRoll) % 10,
										posP2,
										scoreP1 + ((posP1 + diceRoll) % 10) + 1,
										scoreP2,
									].join(",")
								] = 0;
							}
							newUniv[
								[
									(posP1 + diceRoll) % 10,
									posP2,
									scoreP1 + ((posP1 + diceRoll) % 10) + 1,
									scoreP2,
								].join(",")
							] += curUnivN * universesByTripleThrow[diceRoll];
						}
					}
				} else {
					for (let diceRoll = 3; diceRoll < 10; diceRoll++) {
						if (scoreP2 + ((posP2 + diceRoll) % 10) + 1 >= 21) {
							p2wins +=
								curUnivN * universesByTripleThrow[diceRoll];
						} else {
							if (
								newUniv[
									[
										posP1,
										(posP2 + diceRoll) % 10,
										scoreP1,
										scoreP2 + ((posP2 + diceRoll) % 10) + 1,
									].join(",")
								] == undefined
							) {
								newUniv[
									[
										posP1,
										(posP2 + diceRoll) % 10,
										scoreP1,
										scoreP2 + ((posP2 + diceRoll) % 10) + 1,
									].join(",")
								] = 0;
							}
							newUniv[
								[
									posP1,
									(posP2 + diceRoll) % 10,
									scoreP1,
									scoreP2 + ((posP2 + diceRoll) % 10) + 1,
								].join(",")
							] += curUnivN * universesByTripleThrow[diceRoll];
						}
					}
				}
			}
		}
		universes = JSON.parse(JSON.stringify(newUniv));
		p1turn = !p1turn;
	}
	return Math.max(p1wins, p2wins);
}

function printAnswers(puzzle_input) {
	console.table({
		"Part 1 :": solvePartOne(puzzle_input),
		"Part 2 :": solvePartTwo(puzzle_input),
	});
}

parseInput("inputs/day21.txt");
