let reader = require("line-reader");
function parseInput(path) {
	let input = [];
	reader.eachLine(path, function (line, last) {
		input.push(line.split(""));
		if (last) {
			printAnswers(input);
		}
	});
}

function solvePartOne(input) {
	let totalScore = 0;
	for (let i = 0; i < input.length; i++) {
		let opened_Brack = [];
		for (let j = 0; j < input[i].length; j++) {
			const curr_char = input[i][j];
			if (
				curr_char === ">" ||
				curr_char === "}" ||
				curr_char === "]" ||
				curr_char === ")"
			) {
				let curr_oposite = opened_Brack.pop();
				if (curr_char === ">" && curr_oposite !== "<") {
					totalScore += 25137;
					break;
				} else if (curr_char === "}" && curr_oposite !== "{") {
					totalScore += 1197;
					break;
				} else if (curr_char === "]" && curr_oposite !== "[") {
					totalScore += 57;
					break;
				} else if (curr_char === ")" && curr_oposite !== "(") {
					totalScore += 3;
					break;
				}
			} else {
				opened_Brack.push(curr_char);
			}
		}
	}
	return totalScore;
}

function solvePartTwo(input) {
	let scores = [];
	input = input.forEach((line) => {
		let opened_Brack = [];
		let corruptedLine = false;
		for (let j = 0; j < line.length; j++) {
			const curr_char = line[j];
			if (
				curr_char === ">" ||
				curr_char === "}" ||
				curr_char === "]" ||
				curr_char === ")"
			) {
				let curr_oposite = opened_Brack.pop();
				if (curr_char === ">" && curr_oposite !== "<") {
					corruptedLine = true;
					break;
				} else if (curr_char === "}" && curr_oposite !== "{") {
					corruptedLine = true;
					break;
				} else if (curr_char === "]" && curr_oposite !== "[") {
					corruptedLine = true;
					break;
				} else if (curr_char === ")" && curr_oposite !== "(") {
					corruptedLine = true;
					break;
				}
			} else {
				opened_Brack.push(curr_char);
			}
		}
		if (!corruptedLine) {
			let current_score = 0;
			let currChar = opened_Brack.pop();
			while (currChar !== undefined) {
				current_score *= 5;
				if (currChar === "<") {
					current_score += 4;
				} else if (currChar === "{") {
					current_score += 3;
				} else if (currChar === "[") {
					current_score += 2;
				} else if (currChar === "(") {
					current_score += 1;
				}
				currChar = opened_Brack.pop();
			}
			scores.push(current_score);
		}
	});
	scores = scores.sort((a, b) => a - b);
	return scores[(scores.length - 1) / 2];
}

function printAnswers(input) {
	console.table({
		"Part 1 :": solvePartOne(input),
		"Part 2 :": solvePartTwo(input),
	});
}

parseInput("inputs/day10.txt");
