let reader = require("line-reader");
function parseInput(path) {
	let puzzle_input = [];
	reader.eachLine(path, function (line, last) {
		puzzle_input.push(line);
		if (last) {
			printAnswers(puzzle_input);
		}
	});
}

function addSnails(number1, number2) {
	let newNumberArr = ("[" + number1 + "," + number2 + "]").split("");
	newNumberArr = fuseNumbers(newNumberArr);
	let reduced = false;
	while (!reduced) {
		reduced = true;
		let depth_count = 0;
		for (let i = 0; i < newNumberArr.length; i++) {
			if (newNumberArr[i] == "[") {
				depth_count++;
			} else if (newNumberArr[i] == "]") {
				depth_count--;
			}
			if (depth_count == 5) {
				for (let j = i; j >= 0; j--) {
					const currpos = newNumberArr[j];
					if (
						currpos !== "[" &&
						currpos !== "," &&
						currpos !== "]" &&
						currpos !== "" &&
						!Number.isNaN(currpos)
					) {
						newNumberArr[j] =
							+newNumberArr[i + 1] + +newNumberArr[j];
						break;
					}
				}
				for (let j = i + 4; j < newNumberArr.length; j++) {
					const currpos = newNumberArr[j];
					if (
						currpos !== "[" &&
						currpos !== "," &&
						currpos !== "]" &&
						currpos !== "" &&
						!Number.isNaN(currpos)
					) {
						newNumberArr[j] =
							+newNumberArr[i + 3] + +newNumberArr[j];
						break;
					}
				}
				newNumberArr[i] = "0";
				newNumberArr.splice(i + 1, 4);
				reduced = false;
				break;
			}
		}
		if (reduced) {
			for (let i = 0; i < newNumberArr.length; i++) {
				const currpos = newNumberArr[i];
				if (
					currpos !== "[" &&
					currpos !== "," &&
					currpos != "]" &&
					parseInt(currpos) > 9
				) {
					newNumberArr[i] =
						"[" +
						Math.floor(parseInt(currpos) / 2) +
						"," +
						Math.ceil(parseInt(currpos) / 2) +
						"]";
					reduced = false;
					break;
				}
			}
		}
		newNumberArr = fuseNumbers(newNumberArr.join("").split(""));
	}
	return newNumberArr.join("");
}

function fuseNumbers(array) {
	for (let index = 0; index < array.length; index++) {
		if (
			array[index] !== "[" &&
			array[index] !== "," &&
			array[index] !== "]" &&
			array[index] !== "" &&
			!Number.isNaN(parseInt(array[index]))
		) {
			let i = index;
			while (
				array[i] !== "[" &&
				array[i] !== "," &&
				array[i] !== "]" &&
				array[i] !== ""
			) {
				i++;
			}
			array[index] = array.slice(index, i).join("");
			for (let j = index + 1; j < i; j++) {
				array.splice(j, 1);
			}
		}
	}
	return array;
}

function countMagnitude(number) {
	let array = number.split("");
	let leftdepth = 0;
	let rightdepth = 0;
	let totalCount = 0;
	for (let i = 0; i < array.length; i++) {
		const element = array[i];
		if (element == "[") {
			leftdepth += 1;
		} else if (element == ",") {
			leftdepth--;
			rightdepth++;
		} else if (element == "]") {
			rightdepth--;
		} else {
			totalCount +=
				parseInt(element) *
				Math.pow(3, leftdepth) *
				Math.pow(2, rightdepth);
		}
	}

	return totalCount;
}

function solvePartOne(puzzle_input) {
	let mynumber = JSON.parse(JSON.stringify(puzzle_input[0]));
	for (let index = 1; index < puzzle_input.length; index++) {
		const newNumber = JSON.parse(JSON.stringify(puzzle_input[index]));
		mynumber = JSON.parse(JSON.stringify(addSnails(mynumber, newNumber)));
	}
	return countMagnitude(mynumber);
}

function solvePartTwo(puzzle_input) {
	let max = 0;
	for (let index = 0; index < puzzle_input.length; index++) {
		const num1 = JSON.parse(JSON.stringify(puzzle_input[index]));
		for (let j = 0; j < puzzle_input.length; j++) {
			if (index !== j) {
				const num2 = JSON.parse(JSON.stringify(puzzle_input[j]));
				let sum = countMagnitude(addSnails(num1, num2));
				if (sum > max) {
					max = sum;
				}
			}
		}
	}
	return max;
}

function printAnswers(puzzle_input) {
	console.table({
		"Part 1 :": solvePartOne(puzzle_input),
		"Part 2 :": solvePartTwo(puzzle_input),
	});
}

parseInput("inputs/day18.txt");

[854, 959];
