let reader = require("line-reader");
function parseInput(path) {
	let input = [];
	reader.eachLine(path, function (line, last) {
		input = line
			.split(",")
			.map((el) => parseInt(el))
			.sort((a, b) => a - b);
		if (last) {
			printAnswers(input);
		}
	});
}

function distSumP1(point, pointArray) {
	let sum = 0;
	for (let i = 0; i < pointArray.length; i++) {
		sum += Math.abs(point - pointArray[i]);
	}
	return sum;
}

function solvePartOne(input) {
	let current_point = input.reduce((a, b) => a + b, 0) / input.length;
	let min_dist = distSumP1(current_point, input);
	for (let i = 0; i < input[input.length - 1]; i++) {
		let newdist = distSumP1(i, input);
		if (newdist < min_dist) {
			min_dist = newdist;
			current_point = i;
		}
	}
	return min_dist;
}

function distSumP2(point, pointArray) {
	let sum = 0;
	for (let i = 0; i < pointArray.length; i++) {
		let n = Math.abs(point - pointArray[i]);
		sum += (n * (n + 1)) / 2;
	}
	return sum;
}

function solvePartTwo(input) {
	let current_point = input.reduce((a, b) => a + b, 0);
	let min_dist = distSumP2(current_point, input);
	for (let i = 0; i < input[input.length - 1]; i++) {
		let newpoint = i;
		let newdist = distSumP2(i, input);
		if (newdist < min_dist) {
			min_dist = newdist;
			current_point = newpoint;
		}
	}
	return min_dist;
}

function printAnswers(input) {
	console.table({
		"Part 1 :": solvePartOne(input),
		"Part 2 :": solvePartTwo(input),
	});
}

parseInput("inputs/day7.txt");
