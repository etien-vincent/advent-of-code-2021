let reader = require("line-reader");
function parseInput(path) {
	let start;
	let pairs = {};
	reader.eachLine(path, function (line, last) {
		if (start === undefined) {
			start = line;
		} else {
			if (line !== "") {
				pairs[line.split(" -> ")[0]] = line.split(" -> ")[1];
			}
		}
		if (last) {
			printAnswers(start, pairs);
		}
	});
}

function solvePartOne(start, pairs) {
	let curr_str = "" + start;
	for (let index = 0; index < 10; index++) {
		let newStr = "" + curr_str;
		let offset = 0;
		for (let j = 0; j < curr_str.length; j++) {
			if (pairs[curr_str.slice(j, j + 2)] !== undefined) {
				newStr = [
					newStr.slice(0, j + 1 + offset),
					pairs[curr_str.slice(j, j + 2)],
					newStr.slice(j + 1 + offset),
				].join("");
				offset++;
			}
		}
		curr_str = newStr;
	}

	let counts = {};
	for (index = 0, len = curr_str.length; index < len; ++index) {
		ch = curr_str.charAt(index);
		count = counts[ch];
		counts[ch] = count ? count + 1 : 1;
	}
	return counts["P"] - counts["C"];
}

function solvePartTwo(start, pairs) {
	let currentPairs = {};
	let curr_str = "" + start;
	let counts = {};
	for (index = 0, len = curr_str.length; index < len; ++index) {
		ch = curr_str.charAt(index);
		let count = counts[ch];
		counts[ch] = count ? count + 1 : 1;
	}

	for (let j = 0; j < curr_str.length; j++) {
		if (pairs[curr_str.slice(j, j + 2)] !== undefined) {
			if (currentPairs[curr_str.slice(j, j + 2)] === undefined) {
				currentPairs[curr_str.slice(j, j + 2)] = {
					count: 0,
					produces: pairs[curr_str.slice(j, j + 2)],
					p1:
						curr_str.slice(j, j + 2).split("")[0] +
						pairs[curr_str.slice(j, j + 2)],
					p2:
						pairs[curr_str.slice(j, j + 2)] +
						curr_str.slice(j, j + 2).split("")[1],
				};
			}
			currentPairs[curr_str.slice(j, j + 2)].count++;
		}
	}

	for (let index = 0; index < 40; index++) {
		let newCurrentPairs = JSON.parse(JSON.stringify(currentPairs));
		for (const pair in currentPairs) {
			if (Object.hasOwnProperty.call(currentPairs, pair)) {
				if (currentPairs[pair].count > 0) {
					let count = counts[currentPairs[pair].produces];
					counts[currentPairs[pair].produces] = count
						? count + currentPairs[pair].count
						: currentPairs[pair].count;
					newCurrentPairs[pair].count -= currentPairs[pair].count;
					let p1 = currentPairs[pair].p1;
					let p2 = currentPairs[pair].p2;

					if (pairs[p1] !== undefined) {
						if (newCurrentPairs[p1] === undefined) {
							newCurrentPairs[p1] = {
								count: 0,
								produces: pairs[p1],
								p1: p1.split("")[0] + pairs[p1],
								p2: pairs[p1] + p1.split("")[1],
							};
						}
						newCurrentPairs[p1].count += currentPairs[pair].count;
					}
					if (pairs[p2] !== undefined) {
						if (newCurrentPairs[p2] === undefined) {
							newCurrentPairs[p2] = {
								count: 0,
								produces: pairs[p2],
								p1: p2.split("")[0] + pairs[p2],
								p2: pairs[p2] + p2.split("")[1],
							};
						}
						newCurrentPairs[p2].count += currentPairs[pair].count;
					}
				}
			}
		}
		currentPairs = JSON.parse(JSON.stringify(newCurrentPairs));
	}
	return counts["N"] - counts["F"];
}

function printAnswers(start, pairs) {
	console.table({
		"Part 1 :": solvePartOne(start, pairs),
		"Part 2 :": solvePartTwo(start, pairs),
	});
}

parseInput("inputs/day14.txt");
