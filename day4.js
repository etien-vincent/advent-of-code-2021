let reader = require("line-reader");
function parseInput(path) {
	let bingoNumbers;
	let bingoGridIndex = -1;
	let bingoGrids = [];
	reader.eachLine(path, function (line, last) {
		if (!bingoNumbers) {
			bingoNumbers = line.split(",").map((el) => parseInt(el));
		} else {
			if (line === "") {
				bingoGridIndex += 1;
			} else {
				line = line.replace(/  /g, " ").split(" ");
				if (line[0] === "") {
					line.shift();
				}
				if (!bingoGrids[bingoGridIndex]) {
					bingoGrids[bingoGridIndex] = [];
				}
				bingoGrids[bingoGridIndex].push(line.map((el) => parseInt(el)));
			}
			if (last) {
				printAnswers(bingoGrids, bingoNumbers);
			}
		}
	});
}

function gridValid(grid, drawnNumbers) {
	for (let i = 0; i < grid.length; i++) {
		const row = grid[i];
		let rowValid = true;
		for (let j = 0; j < row.length; j++) {
			const value = row[j];
			if (!drawnNumbers.includes(value)) {
				rowValid = false;
				break;
			}
		}
		if (rowValid) {
			return true;
		}
	}
	for (let index = 0; index < grid.length; index++) {
		let columnValid = true;
		for (let j = 0; j < grid.length; j++) {
			if (!drawnNumbers.includes(grid[j][index])) {
				columnValid = false;
			}
		}
		if (columnValid) {
			return true;
		}
	}
}

function gridSum(grid, drawnNumbers) {
	let sum = 0;
	grid.forEach((row) => {
		row.forEach((element) => {
			if (!drawnNumbers.includes(element)) {
				sum += element;
			}
		});
	});
	return sum;
}

function solvePartOne(bingoGrids, bingoNumbers) {
	let drawnNumbers = [];
	let finalNumber;
	for (let i = 0; i < bingoNumbers.length; i++) {
		const currentNumber = bingoNumbers[i];
		drawnNumbers.push(currentNumber);
		for (let j = 0; j < bingoGrids.length; j++) {
			const grid = bingoGrids[j];
			if (gridValid(grid, drawnNumbers)) {
				if (!finalNumber) {
					finalNumber = gridSum(grid, drawnNumbers) * currentNumber;
				}
			}
		}
	}
	return finalNumber;
}

function solvePartTwo(bingoGrids, bingoNumbers) {
	let drawnNumbers = [];
	let finalNumber;
	let validGrids = [];
	for (let i = 0; i < bingoNumbers.length; i++) {
		const currentNumber = bingoNumbers[i];
		drawnNumbers.push(currentNumber);
		for (let j = 0; j < bingoGrids.length; j++) {
			const grid = bingoGrids[j];
			if (gridValid(grid, drawnNumbers)) {
				if (!validGrids.includes(j)) {
					validGrids.push(j);
					finalNumber = gridSum(grid, drawnNumbers) * currentNumber;
				}
			}
		}
	}
	return finalNumber;
}

function printAnswers(bingoGrids, bingoNumbers) {
	console.table({
		"Part 1 :": solvePartOne(bingoGrids, bingoNumbers),
		"Part 2 :": solvePartTwo(bingoGrids, bingoNumbers),
	});
}

parseInput("inputs/day4.txt");
