let reader = require("line-reader");
var Graph = require("graphlib").Graph;
let graphlib = require("graphlib");

function parseInput(path) {
	let puzzle_input = [];
	reader.eachLine(path, function (line, last) {
		puzzle_input.push(line.split("").map((el) => parseInt(el)));
		if (last) {
			printAnswers(puzzle_input);
		}
	});
}
function mooreNeighbourhood(x, y, maxlen) {
	mooreNeighbours = [];

	if (x - 1 >= 0) {
		mooreNeighbours.push({ x: x - 1, y: y });
	}
	if (x + 1 <= maxlen) {
		mooreNeighbours.push({ x: x + 1, y: y });
	}

	if (y - 1 >= 0) {
		mooreNeighbours.push({ x: x, y: y - 1 });
	}
	if (y + 1 <= maxlen) {
		mooreNeighbours.push({ x: x, y: y + 1 });
	}

	return mooreNeighbours;
}

function solvePartOne(puzzle_input) {
	let input = JSON.parse(JSON.stringify(puzzle_input));
	var g = new Graph({ directed: true, multigraph: true });
	for (let i = 0; i < input.length; i++) {
		for (let j = 0; j < input[i].length; j++) {
			const element = input[i][j];
			g.setNode(i + "," + j);
		}
	}

	for (let i = 0; i < input.length; i++) {
		for (let j = 0; j < input[i].length; j++) {
			const element = input[i][j];
			const moorN = mooreNeighbourhood(i, j, input[i].length - 1);
			moorN.forEach((voisin) => {
				g.setEdge(voisin.x + "," + voisin.y, i + "," + j, element);
			});
		}
	}
	function weight(e) {
		return g.edge(e);
	}
	return graphlib.alg.dijkstra(g, "0,0", weight)["99,99"].distance;
}

function addToValue(value, addition) {
	if (value + addition <= 9) {
		return value + addition;
	} else {
		return value + addition - 9;
	}
}

function solvePartTwo(puzzle_input) {
	let input = JSON.parse(JSON.stringify(puzzle_input));
	for (let i = 0; i < input.length; i++) {
		input[i] = [
			...input[i],
			...input[i].map((value) => addToValue(value, 1)),
			...input[i].map((value) => addToValue(value, 2)),
			...input[i].map((value) => addToValue(value, 3)),
			...input[i].map((value) => addToValue(value, 4)),
		];
	}
	let newInput = JSON.parse(JSON.stringify(input));
	for (let index = 1; index < 5; index++) {
		for (let i = 0; i < input.length; i++) {
			newInput.push(input[i].map((value) => addToValue(value, index)));
		}
	}
	input = JSON.parse(JSON.stringify(newInput));

	var g = new Graph({ directed: true, multigraph: true });
	for (let i = 0; i < input.length; i++) {
		for (let j = 0; j < input[i].length; j++) {
			const element = input[i][j];
			g.setNode(i + "," + j);
		}
	}

	for (let i = 0; i < input.length; i++) {
		for (let j = 0; j < input[i].length; j++) {
			const element = input[i][j];
			const moorN = mooreNeighbourhood(i, j, input[i].length - 1);
			moorN.forEach((voisin) => {
				g.setEdge(voisin.x + "," + voisin.y, i + "," + j, element);
			});
		}
	}
	function weight(e) {
		return g.edge(e);
	}
	return graphlib.alg.dijkstra(g, "0,0", weight)["499,499"].distance;
}

function printAnswers(puzzle_input) {
	console.table({
		"Part 1 :": solvePartOne(puzzle_input),
		"Part 2 :": solvePartTwo(puzzle_input),
	});
}

parseInput("inputs/day15.txt");
