let reader = require("line-reader");
function parseInput(path) {
	let point_list = [];
	let fold_list = [];
	let coordinates_mode = true;
	reader.eachLine(path, function (line, last) {
		if (line == "") {
			coordinates_mode = false;
		} else {
			if (coordinates_mode) {
				point_list[line.split(",")[0] + "," + line.split(",")[1]] = {
					x: parseInt(line.split(",")[0]),
					y: parseInt(line.split(",")[1]),
				};
			} else {
				fold_list.push({
					value: parseInt(line.split("=")[1]),
					axis: line.split("=")[0].split("fold along ")[1],
				});
			}
		}

		if (last) {
			printAnswers(point_list, fold_list);
		}
	});
}

function fold(points, value, axis) {
	let newPoints = {};
	for (const point in points) {
		if (Object.hasOwnProperty.call(points, point)) {
			if (points[point][axis] > value) {
				if (axis == "x") {
					newPoints[
						value * 2 -
							points[point]["x"] +
							"," +
							points[point]["y"]
					] = {
						x: value * 2 - points[point]["x"],
						y: points[point]["y"],
					};
				} else {
					newPoints[
						points[point]["x"] +
							"," +
							(value * 2 - points[point]["y"])
					] = {
						x: points[point]["x"],
						y: value * 2 - points[point]["y"],
					};
				}
			} else {
				newPoints[points[point]["x"] + "," + points[point]["y"]] = {
					x: points[point]["x"],
					y: points[point]["y"],
				};
			}
		}
	}
	return newPoints;
}

function solvePartOne(point_list, fold_list) {
	return Object.keys(fold(point_list, fold_list[0].value, fold_list[0].axis))
		.length;
}

function solvePartTwo(point_list, fold_list) {
	let currPoints = point_list;
	for (let i = 0; i < fold_list.length; i++) {
		currPoints = fold(currPoints, fold_list[i].value, fold_list[i].axis);
	}
	let pointsArray = [];
	for (let i = 0; i < 6; i++) {
		pointsArray[i] = [];
		for (let j = 0; j < 40; j++) {
			pointsArray[i][j] = ".";
		}
	}
	for (const point in currPoints) {
		if (Object.hasOwnProperty.call(currPoints, point)) {
			pointsArray[currPoints[point]["y"]][currPoints[point]["x"]] = "#";
		}
	}
	for (let i = 0; i < pointsArray.length; i++) {
		console.log(pointsArray[i].join(""));
	}
	return "JPZCUAUR";
}

function printAnswers(point_list, fold_list) {
	console.table({
		"Part 1 :": solvePartOne(point_list, fold_list),
		"Part 2 :": solvePartTwo(point_list, fold_list),
	});
}

parseInput("inputs/day13.txt");
